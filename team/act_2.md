# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->
# Aspiring Nurse

## Kien Marl Trinidad

👋 Aspiring Nurse! 🚀👨‍💻 — 💌 kienmarltrinidad@student.laverdad.edu.ph — Apalit, Pampanga

![alt act_2_trinidad_kienmarl.jpg](images/act_2_trinidad_kienmarl.jpg)

### Bio

**Good to know:**  I'm a fan of Harry Potter since I was Highschool. Sometimes I like to communicate with people and sometimes I do not. I enjoy playing games because I like to receive and deal with challenging task.

**Motto:** Time is Gold!

**Languages:** Python, Javascript, CSS

**Other Technologies:** Visual Studio Code, Git

**Personality Type:** [Consul INFJ-T](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->
